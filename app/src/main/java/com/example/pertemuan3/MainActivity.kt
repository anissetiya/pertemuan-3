package com.example.pertemuan3

import android.content.AbstractThreadedSyncAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener{

    val COLLECTION = "student"
    val F_ID = "id"
    val F_NAME = "name"
    val F_ADDRESS = "address"
    val F_PHONE = "phone"
    var docId = ""
    lateinit var db : FirebaseFirestore
    lateinit var alStudent : ArrayList<HashMap<String,Any>>
    lateinit var adapter: SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        alStudent = ArrayList()
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClick)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e !=null) Log.d("fireStore", e.message.toString())
            showData()
        }

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                val hm = HashMap<String,Any>()
                hm.set(F_ID,edId.text.toString())
                hm.set(F_NAME,edName.text.toString())
                hm.set(F_ADDRESS,edAddress.text.toString())
                hm.set(F_PHONE,edPhone.text.toString())
                db.collection(COLLECTION).document(edId.text.toString()).set(hm).
                addOnSuccessListener {
                    Toast.makeText(this, "data successfully added",Toast.LENGTH_SHORT)
                        .show()
                }.addOnFailureListener { e ->
                    Toast.makeText(this,
                    "data unsuccessfully addded : ${e.message}",Toast.LENGTH_SHORT)
                    .show()
                }
            }
            R.id.btnUpdate ->{
                val hm = HashMap<String,Any>()
                hm.set(F_ID,docId)
                hm.set(F_NAME,edName.text.toString())
                hm.set(F_ADDRESS,edAddress.text.toString())
                hm.set(F_PHONE,edPhone.text.toString())
                db.collection(COLLECTION).document(docId).update(hm)
                    .addOnSuccessListener { Toast.makeText(this, "data successfully updated",Toast.LENGTH_SHORT)
                        .show()  }
                    .addOnFailureListener { e ->
                        Toast.makeText(this,
                        "data unsuccessfully updated : ${e.message}",Toast.LENGTH_SHORT)
                        .show() }
            }
            R.id.btnDelete ->{
                db.collection(COLLECTION).whereEqualTo(F_ID,docId).get().addOnSuccessListener {
                    result ->
                    for (doc in result){
                        db.collection(COLLECTION).document(doc.id).delete()
                            .addOnSuccessListener {
                                Toast.makeText(this, "data unsuccessfully deleted",Toast.LENGTH_SHORT)
                                    .show()
                            }.addOnFailureListener { e ->
                                Toast.makeText(this,
                                    "data unsuccessfully deleted : ${e.message}",Toast.LENGTH_SHORT)
                                    .show()
                            }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this,
                        "Cant get Data's references : ${e.message}",Toast.LENGTH_SHORT)
                        .show() }
            }
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alStudent.get(position)
        docId = hm.get(F_ID).toString()
        edId.setText(docId)
        edName.setText(hm.get(F_NAME).toString())
        edAddress.setText(hm.get(F_ADDRESS).toString())
        edPhone.setText(hm.get(F_PHONE).toString())
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alStudent.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_NAME, doc.get(F_NAME).toString())
                hm.set(F_ADDRESS, doc.get(F_ADDRESS).toString())
                hm.set(F_PHONE, doc.get(F_PHONE).toString())
                alStudent.add(hm)
            }
            adapter = SimpleAdapter(this,alStudent,R.layout.row_data,
            arrayOf(F_ID,F_NAME,F_ADDRESS,F_PHONE),
            intArrayOf(R.id.txId, R.id.txName, R.id.txAddress, R.id.txPhone))
            lsData.adapter = adapter
        }
    }
}